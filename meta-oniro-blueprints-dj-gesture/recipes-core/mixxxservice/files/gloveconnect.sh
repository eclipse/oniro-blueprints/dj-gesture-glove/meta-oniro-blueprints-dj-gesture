# SPDX-FileCopyrightText: 2021 Huawei Inc.
#
# SPDX-License-Identifier: MIT

#!/usr/bin/expect -f

set prompt "#"
set address [lindex $argv 0]

spawn bluetoothctl
expect -re $prompt
send "remove $address\r"
sleep 1
expect -re $prompt
send "scan on\r"
send_user "\nWaiting for device.\r"
expect -re "\\\[NEW\\\] Device $address"
send_user "\nFound deivce.\r"
sleep 5
send "scan off\r"
sleep 5
expect -re $prompt
send "pair $address\r"
sleep 5
send "connect $address\r"
sleep 5
send "trust $address\r"
sleep 5
send_user "\n$address should be paired now.\r"
send "quit\r"
expect eof
