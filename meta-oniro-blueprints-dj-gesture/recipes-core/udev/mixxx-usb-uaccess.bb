# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT
DESCRIPTION = "Extra udev rule for mixxx usb access"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

inherit allarch

SRC_URI = " \
       file://mixxx-usb-uaccess.rules \
"


do_install() {
    install -d ${D}${sysconfdir}/udev/rules.d
    install -m 0644 ${WORKDIR}/mixxx-usb-uaccess.rules     ${D}${sysconfdir}/udev/rules.d/mixxx-usb-uaccess.rules
}

FILES_${PN} = "${sysconfdir}/udev"
RDEPENDS_${PN} = "udev"
