# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: MIT

EXTRA_OECMAKE:remove = " \
    -DSHOUTCAST=OFF \
    -DLOCALECOMPARE=OFF \
    -DFAAD=ON \
"

EXTRA_OECMAKE += " \
    -DSHOUTCAST=ON \
    -DLOCALECOMPARE=OFF \
    -DFAAD=ON \
"

